package pl.edu.pwr.lostinhashset.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.impl.DefaultUserService;

public class DefaultUserServiceTest {

	private DefaultUserService userService;
	private UserRepository userRepository;
	

	@Before
	public void setUp() {
		userService = new DefaultUserService();
		userRepository = BDDMockito.mock(UserRepository.class);
		userService.setUserRepository(userRepository);
		
		BDDMockito.given(userRepository.getUsers()).willReturn(createTestUsers());
		
	}
	
	private List<User> createTestUsers() {
		List<User> users = new ArrayList<>();
		users.add(new User("John", "Travolta", 17));
		users.add(new User("Scarlett","Johansson", 6));
		users.add(new User("Johnny", "Depp", 29));
		users.add(new User("Nicole","Kidman", 17));
		return users;
	}

	@After
	public void tearDown() {
		userRepository = null;
		userService = null;
	}
	
	@Test
	public void shouldReturnUsersSortedByName() {
		List<User> users = userService.getUsersSortedByName();
		
		for (int i=0, j=1; i<users.size()-1 && j < users.size(); i++, j++) {
			User user1 = users.get(i);
			User user2 = users.get(j);
			System.out.println("User1.name = " + user1.getName() + ", user2.name = " + user2.getName());
			
			Assert.assertTrue(user1.getName().compareTo(user2.getName()) <= 0);
		}
	}
	
	@Test
	public void shouldReturnUsersSortedByPoints() {
		List<User> users = userService.getUsersSortedByPoints();
		
		for (int i=0, j=1; i<users.size()-1 && j < users.size(); i++, j++) {
			User user1 = users.get(i);
			User user2 = users.get(j);
			System.out.println("User1.points = " + user1.getPoints() + ", user2.points = " + user2.getPoints());
			
			Assert.assertTrue(user1.getPoints() <= user2.getPoints());
		}
	}

	
}
