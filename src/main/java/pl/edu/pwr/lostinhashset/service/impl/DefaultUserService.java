package pl.edu.pwr.lostinhashset.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.UserService;

public class DefaultUserService implements UserService {
	
	private UserRepository userRepository;

	@Override
	public List<User> getUsersSortedByName() {
		
		List<User> users = getUserRepository().getUsers();
//		users.sort(new Comparator<User>(){
//
//			@Override
//			public int compare(User arg0, User arg1) {
//				return arg0.getName().compareTo(arg1.getName());
//			
//			}});
		
		users.sort((User arg0, User arg1)->arg0.getName().compareTo(arg1.getName()));
		
		return users;
	}

	@Override
	public List<User> getUsersSortedByPoints() {

		List<User> users = getUserRepository().getUsers();
//		Collections.sort(users, new Comparator<User>(){
//
//			@Override
//			public int compare(User arg0, User arg1) {
//				if(arg0.getPoints() < arg1.getPoints())
//					return -1;
//				else if(arg0.getPoints() > arg1.getPoints())
//					return 1;
//				else
//					return 0;
//			
//			}});
		Collections.sort(users, (User arg0, User arg1)->arg0.getPoints()-arg1.getPoints());
		return users;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

}
